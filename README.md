[![Blender](https://img.shields.io/badge/blender-3.0.0-%23F5792A.svg?style=flat&logo=blender&logoColor=white)](https://www.blender.org/download/)
[![pipeline status](https://gitlab.com/stilobique/blender-docker/badges/main/pipeline.svg)](https://gitlab.com/stilobique/blender-docker/-/commits/main)  


# Blender Docker
This repository is an automated blender docker image file generated, it's usefully with blender addon unit test. To 
build a new specific image, you need to update the `build.json` file with the new version witched ; or 


# Adding an Addon
Mount a new volume with your addon inside a folder (ex `/blender-plugin/plugin-archive.zip`), when you run your docker container add this volume and execute a script to add-it with the blender installed.

Python function to install an addon with Blender. The file his named `install.py`
````python
import bpy
def b3d_install_addon():
    bpy.ops.preferences.addon_install(filepath='./plugin-archive.zip')
    bpy.ops.preferences.addon_enable(module='plugin-name')

if __name__ == '__main__':
    b3d_install_addon()
````

Call this function with blender inside this container
````shell
#!/bin/sh
/opt/blender/blender --background --python "install.py"
````