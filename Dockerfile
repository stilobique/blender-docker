FROM ubuntu:22.04 AS blender

# Setup all software version request
ARG b3d_vs_major=2.90
ARG b3d_vs_minor=0
ARG subversion=63014

LABEL Author="stilobique"
LABEL Title="Blender Docker for Unit Test"

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    gcc \
    git \
    cmake \
    libc6-dev \
    libx11-dev \
    subversion \
    libxrandr-dev \
    libxcursor-dev \
    libxxf86vm-dev \
    libxinerama-dev \
    libglew-dev \
    libxi-dev \
    python3

# Debug
RUN echo Build Blender v${b3d_vs_major}.${b3d_vs_minor}

ADD . /opt/blender-git

# Compile Blender
RUN cd /opt/blender-git/ \
    && git checkout v${b3d_vs_major}.${b3d_vs_minor} \
    && git config --global user.email "contact@aurelien-vaillant.net" \
    && git config --global user.name "Aurelien Vaillant" \
    && mkdir /opt/blender-git/lib \
    && cd /opt/blender-git/lib \
    && svn checkout -r${subversion} \
    && make

# Setup a Multistage optimisation
FROM ubuntu:22.04
COPY --from=blender /opt/blender-git/build_linux/bin /opt/blender
ARG b3d_vs_major
ENV B3D_ADDON_PATH "$HOME/.config/blender/${b3d_vs_major}/scripts/addons"

RUN apt-get update && apt-get install -y \
    libxi6 \
    libxxf86vm1 \
    libxrender1 \
    libgl1-mesa-glx

# Working Directory setup
WORKDIR /