import os
import pathlib
import json
import docker
import sys

# TODO Update readme file badge with blender version
# TODO add a check if the image build are stored in the docker register


def get_data_from_json() -> (str, str):
    """From the json file, get blender version (major and minor), and return this information."""
    file = pathlib.Path(os.getcwd(), 'build.json')

    with open(file, 'r') as f:
        data = json.load(f)
        f.close()

    version_values = data['tags'][0]
    major, minor = version_values['b3d_vs_major'], version_values['b3d_vs_minor']

    return major, minor


def build_docker(b3d_version: (str, str), latest: bool = False):
    """
    Generate docker image, request the blender version (major, minor) with the keyword b3d_version, and can be
    tagged with latest or only blender version info.
    """
    client = docker.from_env()
    if os.environ.get('docker_hub_password'):
        client.login(username=os.environ['docker_hub_user'], password=os.environ['docker_hub_password'])

    # Init all data to build the docker image
    args = {
        'b3d_vs_major': str(b3d_version[0]),
        'b3d_vs_minor': str(b3d_version[1]),
    }
    docker_repo = 'stilobique/blender'
    if latest:
        tag = 'latest'
    else:
        tag = f'{args["b3d_vs_major"]}.{args["b3d_vs_minor"]}'
    completed_tag_name = f'{docker_repo}:{args["b3d_vs_major"]}.{args["b3d_vs_minor"]}'

    print('\t1 -> Build from image file')
    client.images.build(path=os.getcwd(), buildargs=args, tag=completed_tag_name, rm=True, forcerm=True)

    print(f'\t2 -> Push the new images with tag {tag}')
    push = client.images.push(repository=docker_repo, tag=tag, stream=True)
    print(f'\t\tDebug push :\n\t\t{push}')


if __name__ == "__main__":
    version = get_data_from_json()
    print(f'Start -> Setup the package with blender version "{version[0]}.{version[1]}"')
    for arg in sys.argv:
        if '--tag=' in arg:
            print(f'Build with the tag "{arg}".')
            build_docker(b3d_version=version, latest=True)
            break
        else:
            build_docker(b3d_version=version)
            print(f'Start to build the docker file.')
            pass
